<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/favicon.png')}}" />

    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/home.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

</head>

<body id="body">

<div id="preloader">
    <div class='preloader'>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>

@include('navigation')
<section class="pages-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Contact Us</h2>
                <ol class="breadcrumb header-bradcrumb">
                    <li><a href="{{route('home')}}">Home</a></li>
                    <li class="active">Contact Us</li>
                </ol>
            </div>
        </div>
    </div>
</section>


<section class="contact-us" id="contact">
    <div class="container">
        <div class="row">
            <div class="title text-center" style="padding-bottom: 0;">
                <h2>Have a Question?</h2>
                <div class="border"></div>
            </div>
        </div>
    </div>
</section>>

<section class="contact-info">
    <div class="container-fluid">
        <div class="contact-content">
            <div class="row" style="padding-top: 2%;">
                <div class="col-6 col-lg-3">
                    <div class="contact-title">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <h4>Phone</h4>
                        <p>(+977)981-0117399</p>
                    </div>
                </div>
                <div class="col-6 col-lg-3">
                    <div class="contact-title">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        <h4>Address</h4>
                        <p>Chaksibari Marg, Thamel, kathmandu</p>
                    </div>
                </div>
                <div class="col-6 col-lg-3">
                    <div class="contact-title">
                        <i class="fa fa-clock" aria-hidden="true"></i>
                        <h4>Open time</h4>
                        <p>10:00 am to 6:00 pm</p>
                    </div>
                </div>
                <div class="col-6 col-lg-3">
                    <div class="contact-title">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                        <h4>Email</h4>
                        <p>ityashri@gmail.com</p>
                    </div>
                </div>
            </div>

            <div class="google-maps">
                <iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=Yashri%20Pvt.Ltd%2C%20Kathmandu&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
            </div>
        </div>
    </div>
</section>
<div class="contact-form-area section-padding-100" style="margin-bottom: 5vh;">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-heading title text-center" data-wow-delay="100ms">
                    <h2>Leave Message</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="contact-form">
                    <form class="gform" action="https://script.google.com/macros/s/AKfycbzenuC5cwbUUV88ZXNsZWDeXNTvuo_SEXzskh_DPA/exec" method="POST">
                        <div class="thankyou_message">
                            <h2><em>Thanks</em> for contacting us! We will get back to you soon!
                            </h2>
                        </div>
                        <div class="row">
                            <div class="col-12 col-lg-6" data-wow-delay="100ms">
                                <input type="text" name="name" class="form-control mb-30" placeholder="Your Name">
                            </div>
                            <div class="col-12 col-lg-6" data-wow-delay="100ms">
                                <input type="email" name="email" class="form-control mb-30" placeholder="Your Email">
                            </div>
                            <div class="col-12" data-wow-delay="100ms">
                                <textarea name="message" class=" mb-30" placeholder="Your Message"></textarea>
                            </div>
                            <div class="col-12 col-lg-12 text-center" data-wow-delay="100ms">
                                <button type="submit" class="btn btn mt-15">Send Message</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')


<script src="{{asset('jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/smooth-scroll.min.js')}}"></script>
<script src="{{asset('js/script.js')}}"></script>
<script data-cfasync="false" src="{{asset('js/contact.js')}}"></script>

</body>
</html>