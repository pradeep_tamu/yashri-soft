<header class="navigation navbar navbar-fixed-top">
    <div class="container">
        <div class="navbar-header" style="padding-bottom: 10px">

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand logo" href="{{route('home')}}">
                 <img class="white-logo" src="images/yashrilogo_white.png" alt="logo" style="width:100px"/>
            </a>
            <a class="navbar-brand logo" href="{{route('home')}}" style="padding-bottom: 10px; margin-left: -15px;">
                 <img class="blue-logo" src="images/yashri.png" alt="logo" style="width: 102px; height: 55px;"/>
            </a>

        </div>

        <nav class="collapse navbar-collapse navbar-right ">
            <ul id="nav" class="nav navbar-nav menu">
                <li><a href="{{route('home')}}">Home</a></li>
                <li><a href="{{Route('service')}}">Services</a></li>
                <li><a href="{{Route('portfolio')}}">Portfolio</a></li>
                <li><a href="{{Route('about')}}">About Us</a></li>
                <li><a href="{{Route('contact')}}">Contact</a></li>
                </li>
            </ul>
        </nav>

    </div>
</header>