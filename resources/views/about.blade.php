<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/favicon.png')}}"/>


    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/home.css')}}">
    <link rel="stylesheet" href="{{asset('css/about.css')}}">

</head>

<body id="body">

<div id="preloader">
    <div class='preloader'>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>

@include('navigation')

<section class="pages-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>About Us</h2>
                <ol class="breadcrumb header-bradcrumb">
                    <li><a href="{{route('home')}}">Home</a></li>
                    <li class="active">About Us</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="section-sm">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-20 text-center">
                <h2 style="font-size: 40px;
                    color: rgba(0,0,0,0.7);"> Who Are We </h2><br>
                <p>
                    We are a professional web design and development company offering affordable web solutions to small,
                    medium and large scale businesses. We have a highly creative and expert team of PHP Web Programmers
                    for web application development and application services. We take great pride in producing
                    exceptional web sites that effectively marry technology and design. Our unique blend of expert
                    developer, functional analysts, and creative designers ensure that your application will not only
                    look good but will be easy to use, fast, and highly functional.
                </p><br><br>
                <p>
                    The main motive of our company is to build innovative and scalable cost enterprise solutions
                    that helps to achieve business excellence and growth. Our professionals team adopt latest
                    technologies
                    to develop a good project to add unique features to your enterprise.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="section-sm bg-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2 style="color: rgba(0,0,0,0.7);">Our Mission</h2>
                <p>Established with the mission to provide technology enabled high quality products, services and
                    solution, Yashri now provides its services to wide customers including major governmental
                    organizations, trading houses, and private agencies.</p>
                <h2 style="color: rgba(0,0,0,0.7);">Our Vision</h2>
                <p>To develop software applications that can be used by all users globally.
                    To have a team with highly creative, hard working and dedicated people.
                    To partner and associate with world class companies.
                    To have systematic, efficient and quality management system in all process within the
                    organization.</p>
            </div>
            <div class="col-md-6" style="height: 50vh;">
                <img src="images/mission.svg" alt="" class="img-responsive mt-30" style="height: 100%; width: 100%;">
            </div>
        </div>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="heading-title text-center">
            <h2 class="text-uppercase">Our professionals </h2>
        </div>

        <div class="col-md-4 col-sm-4">
            <div class="team-member">
                <div class="team-img">
                    <img src="{{asset('images/ceo.jpg')}}" alt="team member" class="img-responsive">
                </div>
                <div class="team-hover">
                    <div class="desk">
                        <h4>Founder &amp; CEO</h4>
                    </div>
                    <div class="s-link">
                        <a href="#"><i class="fab fa-instagram"></i></a>
                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                    </div>
                </div>
            </div>
            <div class="team-title">
                <h5>Ricky Dhungana</h5>
                <span>founder &amp; ceo</span>
            </div>
        </div>
        <div class="col-md-4 col-sm-4">
            <div class="team-member">
                <div class="team-img">
                    <img src="{{asset('images/team8.jpg')}}" alt="team member" class="img-responsive">
                </div>
                <div class="team-hover">
                    <div class="desk">
                        <h4>Program Director</h4>
                    </div>
                    <div class="s-link">
                        <a href="#"><i class="fab fa-instagram"></i></a>
                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                    </div>
                </div>
            </div>
            <div class="team-title">
                <h5>Amit Tiwari</h5>
                <span>Program Director</span>
            </div>
        </div>
        <div class="col-md-4 col-sm-4">
            <div class="team-member">
                <div class="team-img">
                    <img src="{{asset('images/team4.jpg')}}" alt="team member" class="img-responsive">
                </div>
                <div class="team-hover">
                    <div class="desk">
                        <h4>Graphic Designer</h4>
                    </div>
                    <div class="s-link">
                        <a href="#"><i class="fab fa-instagram"></i></a>
                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                    </div>
                </div>
            </div>
            <div class="team-title">
                <h5>Sinora Khadka</h5>
                <span>Graphic Designer</span>
            </div>
        </div>

        <div class="col-md-4 col-sm-4">
            <div class="team-member">
                <div class="team-img">
                    <img src="{{asset('images/team3.jpg')}}" alt="team member" class="img-responsive">
                </div>
                <div class="team-hover">
                    <div class="desk">
                        <h4>SEO & Content Writer</h4>
                    </div>
                    <div class="s-link">
                        <a href="#"><i class="fab fa-instagram"></i></a>
                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                    </div>
                </div>
            </div>
            <div class="team-title">
                <h5>Alisha Sigdel</h5>
                <span>SEO & Content Writer</span>
            </div>
        </div>

        <div class="col-md-4 col-sm-4">
            <div class="team-member">
                <div class="team-img">
                    <img src="{{asset('images/team5.jpg')}}" alt="team member" class="img-responsive">
                </div>
                <div class="team-hover">
                    <div class="desk">
                        <h4>SEO & Content Writer</h4>
                    </div>
                    <div class="s-link">
                        <a href="#"><i class="fab fa-instagram"></i></a>
                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                    </div>
                </div>
            </div>
            <div class="team-title">
                <h5>Shweta Shrestha</h5>
                <span> SEO & Content Writer</span>
            </div>
        </div>
        <div class="col-md-4 col-sm-4">
            <div class="team-member">
                <div class="team-img">
                    <img src="{{asset('images/team6.jpg')}}" alt="team member" class="img-responsive">
                </div>
                <div class="team-hover">
                    <div class="desk">
                        <h4>SEO & Content Writer</h4>
                    </div>
                    <div class="s-link">
                        <a href="#"><i class="fab fa-instagram"></i></a>
                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                    </div>
                </div>
            </div>
            <div class="team-title">
                <h5>Sushmita Basnet</h5>
                <span>SEO & Content Writer</span>
            </div>
        </div>

        <div class="col-md-4 col-sm-4">
            <div class="team-member">
                <div class="team-img">
                    <img src="{{asset('images/team9.jpg')}}" alt="team member" class="img-responsive">
                </div>
                <div class="team-hover">
                    <div class="desk">
                        <h4>Laravel Developer</h4>
                    </div>
                    <div class="s-link">
                        <a href="#"><i class="fab fa-instagram"></i></a>
                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                        <a href="https://www.linkedin.com/in/santosh-bhulun-aaa167118/"><i class="fab fa-linkedin"></i></a>
                    </div>
                </div>
            </div>
            <div class="team-title">
                <h5>Santosh Bhulun Tamang</h5>
                <span>Laravel Developer</span>
            </div>
        </div>

        <div class="col-md-4 col-sm-4">
            <div class="team-member">
                <div class="team-img">
                    <img src="{{asset('images/team7.jpg')}}" alt="team member" class="img-responsive">
                </div>
                <div class="team-hover">
                    <div class="desk">
                        <h4>Laravel Developer</h4>
                    </div>
                    <div class="s-link">
                        <a href="#"><i class="fab fa-instagram"></i></a>
                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                    </div>
                </div>
            </div>
            <div class="team-title">
                <h5>Bivek Shrestha</h5>
                <span>Laravel Developer</span>
            </div>
        </div>
        <div class="col-md-4 col-sm-4">
            <div class="team-member">
                <div class="team-img">
                    <img src="{{asset('images/team2.jpg')}}" alt="team member" class="img-responsive">
                </div>
                <div class="team-hover">
                    <div class="desk">
                        <h4>Laravel Developer</h4>
                    </div>
                    <div class="s-link">
                        <a href="#"><i class="fab fa-instagram"></i></a>
                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                    </div>
                </div>
            </div>
            <div class="team-title">
                <h5>Manish Maharjan</h5>
                <span>Laravel Developer</span>
            </div>
        </div>
        <div class="col-md-4 col-sm-4">
            <div class="team-member">
                <div class="team-img">
                    <img src="{{asset('images/team1.jpeg')}}" alt="team member" class="img-responsive">
                </div>
                <div class="team-hover">
                    <div class="desk">
                        <h4>Full Stack Developer</h4>
                    </div>
                    <div class="s-link">
                        <a href="#"><i class="fab fa-instagram"></i></a>
                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                        <a href="https://www.linkedin.com/in/pradeep-gurung-142833184/"><i class="fab fa-linkedin"></i></a>
                    </div>
                </div>
            </div>
            <div class="team-title">
                <h5>Pradeep Gurung</h5>
                <span>Full Stack Developer</span>
            </div>
        </div>



    </div>

</div>


@include('footer')

<script src="{{asset('jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/smooth-scroll.min.js')}}"></script>
<script src="{{asset('js/script.js')}}"></script>
</body>
</html>