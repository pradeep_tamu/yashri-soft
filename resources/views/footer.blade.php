<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
      integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

<footer id="footer" class="bg-one">
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-3">
                    <a href="{{route('home')}}">
                        <img src="images/yashrilogo_white.png" alt="logo" style="width:150px; margin-bottom: 2vh;"/>
                    </a>
                    <p>Yashri is the IT consulting & software solutions trying to provide different combinations of the
                        professionals to provide agile,web,mobile and cloud based solutions.</p>
                </div>

                <div class="col-sm-3 col-md-3 col-lg-3">
                    <ul>
                        <li><h3>Our Services</h3></li>
                        <li><a href="{{route('service')}}">Web Application</a></li>
                        <li><a href="{{route('service')}}">Mobile Application</a></li>
                        <li><a href="{{route('service')}}">Graphic Design</a></li>
                        <li><a href="{{route('service')}}">Digital Marketing</a></li>
                    </ul>
                </div>


                <div class="col-sm-3 col-md-3 col-lg-3">
                    <h3>Contact Us</h3>
                    <p>
                        Chaksibari Marg <br>
                        Thamel, Kathmandu<br>
                        Nepal<br>
                        <strong>Phone:</strong> (+977)981-0117399<br>
                        <strong>Email:</strong>ityashri@gmail.com<br>
                    </p>
                    <i class="fab fa-instagram"></i>
                    <i class="fab fa-facebook-f"></i>
                    <i class="fab fa-twitter"></i>

                </div>
                <div class="col-sm-3 col-md-3 col-lg-3">
                    <div class="footer-subscribe mb-80">
                        <h3 class="subscribe-title">Subscribe</h3>
                        <p>Subscribe our newsletter gor get notification about new updates.</p>

                        <form action="" class="subscribe-form">
                            <input type="email" class="form-control" placeholder="Enter your email...">
                            <button type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

</footer>