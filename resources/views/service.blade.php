<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/favicon.png')}}" />


    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/home.css')}}">
</head>

<body id="body">


<div id="preloader">
    <div class='preloader'>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>

@include('navigation')

<section class="pages-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Our Services</h2>
                <ol class="breadcrumb header-bradcrumb">
                    <li><a href="{{route('home')}}">Home</a></li>
                    <li class="active">Our Services</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="services"  id="services">
    <div class="container">
        <div class="row">

            <div class="title text-center" >
                <h2 >Our Services</h2>
                <p>We are committed to deliver cost-
                    effective high quality solutions by integrating the most advanced technologies through empowering
                    professionals and client-focused approach.</p>
                <div class="border"></div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12  no-padding" >
                <div class="service-block color-bg text-center">
                    <div class="service-icon text-center">
                        <i class="fab fa-searchengin"></i>
                    </div>
                    <h3>SEO</h3>
                    <p>Rank your business at the top of Search Engines. Get found at the right time, in the right place, by the right people.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12  no-padding" >
                <div class="service-block text-center">
                    <div class="service-icon text-center">
                        <i class="fas fa-laptop-medical"></i>
                    </div>
                    <h3>Responsive Design</h3>
                    <p>We create attractive designs to valid cross browser compatible to meet your requirement and current trends.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12  no-padding"  >
                <div class="service-block color-bg text-center">
                    <div class="service-icon text-center">
                        <i class="fas fa-bullhorn"></i>
                    </div>
                    <h3>Digital Marketing</h3>
                    <p>Generate awareness, engage users, and boost conversions. Speak to your audience directly, but better.</p>
            </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12  no-padding" >
                <div class="service-block  text-center">
                    <div class="service-icon text-center">
                        <i class="fas fa-paint-brush"></i>
                    </div>
                    <h3>Graphic Design</h3>
                    <p>Our team delivers you a supreme designs of banner, posts, brochures and all the essential promotional digital designs.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12  no-padding"  >
                <div class="service-block color-bg text-center">
                    <div class="service-icon text-center">
                        <i class="fab fa-dev"></i>
                    </div>
                    <h3>Web Application</h3>
                    <p>Our team of experts are continuously involved in connecting the back-end and front-end of web development to ensure your strong web identity.</p>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12  no-padding">
                <div class="service-block text-center">
                    <div class="service-icon text-center">
                        <i class="fab fa-app-store-ios"></i>
                    </div>
                    <h3>Mobile Application</h3>
                    <p>We develop powerful, highly usable mobile applications that work seamlessly on any device that solve business problems, attract users, and reinforce your brand.</p>
                </div>
            </div>

        </div>
    </div>
</section>

@include('footer')

<script src="{{asset('jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/smooth-scroll.min.js')}}"></script>
<script src="{{asset('js/script.js')}}"></script>

</body>
</html>