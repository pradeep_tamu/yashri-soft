<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Yashri</title>


    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">

    <link rel="stylesheet" href="{{asset('css/home.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">



</head>
<body>

@include('navigation')

<section class="landing">
    <div id="particles-js"></div>
    <div class="container">
        <div class="row">

            <div class="col-md-12">

                <div class="block">

                    <h1 >DON'T LOOK<br> ANYWHERE</h1>
                    <p>Yashri is the IT consulting & software solutions trying to provide different combinations of the
                        professionals to provide agile,web,mobile and cloud based solutions.</p>
                    <ul class="list-inline">
                        <li>
                            <a  href="{{route('service')}}" class="btn btn-main">Explore Us</a>
                        </li>
                        <li>
                            <a  href="{{route('about')}}" class="btn btn-transparent">Learn More</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="offer-service section">
    <div class="container">
        <div class="row">

            <div class="title text-center">
                <h2>What Do We Offer</h2>
                <p>We deliver high-quality services including web design, web development and digital marketing. We will guide you via the best methods in designing user experience and interface. </p>
                <div class="border"></div>
            </div>


            <div class="col-md-12">
                <div class="row text-center">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="service-item">
                            <i class="fas fa-cogs"></i>
                            <h4>Web Development</h4>
                            <p>Web application provides generally a universal solution to address your company to the outsiders. We build and host premium websites from conception to implementation.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="service-item">
                            <i class="fas fa-diagnoses"></i>
                            <h4>Graphic Design</h4>
                            <p>Graphic Design gives your Company a Face and Visual Presentation that just by looking at it; you have a feelings and mental positioning in mind on the product.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="service-item">
                            <i class="fas fa-quidditch"></i>
                            <h4>UI/UX Design</h4>
                            <p>UX Design relates to the wireframes, storyboards and sitemap whereas UI Design deals with the overall layout and look & feel of an app</p>
                        </div>
                    </div>>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="service-item">
                            <i class="fab fa-app-store-ios"></i>
                            <h4>App Development</h4>
                            <p>We develop powerful, highly usable cross platform mobile applications that work seamlessly on any device that solve business problems, attract users, and reinforce your brand.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="service-item">
                            <i class="fas fa-bullhorn"></i>
                            <h4>Digital Marketing</h4>
                            <p>Create a brand that stays relevant among your fanbase and procures consistent results.Generate awareness, engage users, and boost conversions. Speak to your audience directly, but better.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="service-item">
                            <i class="fab fa-searchengin"></i>
                            <h4>Search Engine Optimization (SEO)</h4>
                            <p>Excellent website is of no use if it is not marketed in right way. Rank your business at the top of Search Engines. Get found at the right time, in the right place, by the right people. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about-us section">
    <div class="container">
        <div class="row">
            <div class="title text-center">
                <h1>We Are Yashri</h1>
                <p>Yashri is the IT consulting & software solutions trying to provide different combinations of the professionals to provide agile,web,mobile and cloud based solutions. Yashri is committed to deliver cost-
                    effective high quality solutions by integrating the most advanced technologies through empowering professionals and client-focused approach.</p>
                <div class="border"></div>
            </div>

            <div class="col-md-6">
                <img src="images/landing1.svg" class="img-responsive" alt="">
            </div>
            <div class="col-md-6">
                <h2>Why Choose Us</h2>
                <ul class="benefits">
                    <li>We provide our clients with the opportunity to express their innovative ideas to customized their
                        needs-based concept.
                    </li>
                    <li>We design, develop and maintain applications that are customized to meet your core business
                        requirements.</li>
                    <li>With our experience building standards-based websites, your business appears all over the
                        World Wide Web.
                    </li><li>We provide long term retrieval support system that will help your system durability.
                    </li>

                </ul>
                <a href="{{route('service')}}" class="btn btn-main mt-20">Learn More</a>
            </div>
        </div>
    </div>
</section>

<section class="create-something section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Let's Create Something Together</h2>
                <p>We work with you to craft beautiful and unique experiences. With years of knowledge and expertise, we design and deliver custom products and campaigns. We build brands and help them succeed! Drop us a line below to work with us or to find out more.</p>
                <a href="{{route('contact')}}" class="btn btn-main">Contact Us</a>
            </div>
        </div>
    </div>
</section>

<section class=" section-sm">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="title">
                    <h2>Our Achievements</h2>
                    <p>“If you want to achieve excellence, you can get there today. As of this second, quit doing less-than-excellent work.”</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 text-center ">
                <div class="achievements">
                    <i class="far fa-smile-beam"></i>
                    <div>
                        <span data-count="20">20+</span>
                    </div>
                    <h3>Happy Clients</h3>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-6 text-center ">
                <div class="achievements">
                    <i class="fas fa-clipboard-list"></i>
                    <div>
                        <span data-count="30">25+</span>
                    </div>
                    <h3>Projects completed</h3>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-6 text-center ">
                <div class="achievements">
                    <i class="fas fa-comment-dots"></i>
                    <div>
                        <span data-count="40">40+</span>
                    </div>
                    <h3>Positive feedback</h3>

                </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-6 text-center ">
                <div class="achievements kill-border">
                    <i class="fab fa-app-store-ios"></i>

                    <div>
                        <span data-count="6">5+</span>
                    </div>
                    <h3>Mobile Applications</h3>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="member section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="member-slider">
                    <div class="item text-center">
                        <div class="member-info">
                            <p>At Yashri, our best feature is our global team of highly professional and knowledgeable individuals, who, through their hard work, passion, dedication and perceptiveness, continuously drive our firm forward.</p>
                        </div>
                        <div class="member-photo">
                            <img src="{{asset('images/ceo.jpg')}}" class="img-responsive" alt="" style="height: 20vh; width: 20vh; object-fit: cover">
                        </div>
                        <div class="member-post">
                            <h3 >Ricky Dhungana</h3>
                            <span >CEO , Yashri</span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('footer')

<script src="{{asset('jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/smooth-scroll.min.js')}}"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/script.js')}}"></script>
<script src="{{asset('js/particles/particles.js')}}"></script>
<script src="{{asset('js/particles/app.js')}}"></script>
<script>

    $(document).ready(function(){
        $(".owl-carousel").owlCarousel();
    });

    var count_particles, stats, update;

    document.body.appendChild(stats.domElement);
    count_particles = document.querySelector('.particlesjs-count-particles');
    update = function () {
        stats.begin();
        stats.end();
        if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {
            count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
        }
        requestAnimationFrame(update);
    };
    requestAnimationFrame(update);

</script>

</body>
</html>