<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/favicon.png')}}"/>



    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="{{asset('css/home.css')}}">
</head>

<body id="body">

<!--
 Start Preloader
 ==================================== -->
<div id="preloader">
    <div class='preloader'>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!--
End Preloader
==================================== -->


@include('navigation')

<section class="pages-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Portfolio</h2>
                <ol class="breadcrumb header-bradcrumb">
                    <li><a href="index.html">Home</a></li>
                    <li class="active">Portfolio</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="portfolio section-sm" id="portfolio">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-lg-12">

                <!-- section title -->
                <div class="title text-center">
                    <h2>Our Works</h2>
                    <p>A diverse team of highly-experienced professionals with the skills and the passion to push the boundaries of market research and technology. </p>
                    <div class="border"></div>
                </div>
                <!-- /section title -->

                <div class="portfolio-filter">
                    <button class="btn-main" data-filter="all">All</button>
                    <button class="btn-main" data-filter=".ios">Mobile App</button>
                    <button class="btn-main" data-filter=".web">Web Application</button>
                </div>


                <div class="portfolio-items-wrapper">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-6 mix design ios">
                            <div class="portfolio-block">
                                <img class="img-responsive" src="images/global.jpg" alt="">
                                <div class="caption">
                                    <h4><a href="">Globalized Enterprises</a></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6 mix photography web">
                            <div class="portfolio-block">
                                <img class="img-responsive" src="images/fashion.jpg" alt="">
                                <div class="caption">
                                    <h4><a href="">Fashion For Rent</a></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6 mix web">
                            <div class="portfolio-block">
                                <img class="img-responsive" src="images/messenger.jpg" alt="">
                                <div class="caption">
                                    <h4><a href="">Messenger Travels and Tours</a></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div> <!-- /end col-lg-12 -->
        </div> <!-- end row -->
    </div>    <!-- end container -->
</section>


@include('footer')

<script src="{{asset('jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/mixitup.min.js')}}"></script>
<script src="{{asset('js/smooth-scroll.min.js')}}"></script>
<script src="{{asset('js/script.js')}}"></script>

</body>
</html>